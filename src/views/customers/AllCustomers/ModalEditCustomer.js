import {
    Alert,
    Box,
    Button,
    Input,
    MenuItem,
    Modal,
    Select,
    Snackbar,
    Typography,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { Col, Row } from 'reactstrap'
const dtfUS = new Intl.DateTimeFormat('uk', { day: '2-digit', month: '2-digit', year: 'numeric' })
function ModalEditCustomer ({rowClicked, setRowClicked, getData, setVarRefeshPage, varRefeshPage, openModalEditCustomer, onCloseModalEdit}) {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        p: 4,
        borderRadius: '10px',
    }
    const [nameCustomer, setNameCustomer] = useState("")
    const [phoneNumber, setphoneNumber] = useState("")
    const [address, setAddress] = useState("")
    const [email, setEmail] = useState("")
    const [city, setCity] = useState("")
    const [country, setCountry] = useState("")
    const [timeUpdated, setTimeUpdated] = useState("")

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState('')
    const [statusModal, setStatusModal] = useState('error')
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const handleCancelModalClick = () => {
        onCloseModalEdit()
        setNameCustomer("")
        setphoneNumber("")
        setAddress("")
        setEmail("")
        setCity("")
        setCountry("")
        setTimeUpdated("")
        setRowClicked(null)
    }
    const onNameCustomerChange = (event) => {
        setNameCustomer(event.target.value)
    }
    const onPhoneNumberChange = (event) => {
        setphoneNumber(event.target.value)
    }
    const onAddressChange = (event) => {
        setAddress(event.target.value)
    }
    const onCityChange = (event) => {
        setCity(event.target.value)
    }
    const onCountryChange = (event) => {
        setCountry(event.target.value)
    }
    const onEditCustomerClick = () => {
        console.log("nameCustomer", nameCustomer)
        console.log("phoneNumber", phoneNumber)
        console.log("address", address)
        console.log("city", city)
        console.log("country", country)
        var vCheckForm = validateForm();
        if(vCheckForm){
            getApiUpdateCustomer()
        }

    }
    const validateForm = () => {
        const filterPhone = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
        const filterPhone2 = /(([02]))+([0-9]{9})\b/
        if(nameCustomer === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Tên khách hàng cần được nhập')
            setStatusModal('error')
            return false
        }
        if(!(filterPhone.test(phoneNumber) || filterPhone2.test(phoneNumber))){
            setOpenAlert(true)
            setNoidungAlertValid('Sai định dạng số điện thoại')
            setStatusModal('error')
            return false
        }
        if(address === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Địa chỉ cần được nhập')
            setStatusModal('error')
            return false
        }
        if(city === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Thành phố cần được nhập')
            setStatusModal('error')
            return false
        }
        if(country === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Quốc gia cần được nhập')
            setStatusModal('error')
            return false
        }
        else{
            return true;
        }
    }
    const getApiUpdateCustomer = () => {
        var vDataInfoCustomer = {
            fullName: nameCustomer,
            phoneNumber: phoneNumber,
            address: address,
            city: city,
            country: country
        }
        const body = {
            method: 'PUT',
            body: JSON.stringify(vDataInfoCustomer),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("https://shop24h-backend.herokuapp.com/customers/" + rowClicked._id, body)
            .then((data) => {
                setStatusModal("success");
                setNoidungAlertValid("Cập nhật thành công!");
                setOpenAlert(true);
                handleCancelModalClick();
            })
            .catch((error) => {
                setStatusModal("error");
                setNoidungAlertValid("Lỗi mạng, xin thử lại!");
                setOpenAlert(true);
                console.log(error)
            })
    }
    useEffect(() => {
        if(rowClicked){
            setNameCustomer(rowClicked.fullName)
            setphoneNumber(rowClicked.phoneNumber)
            setAddress(rowClicked.address)
            setEmail(rowClicked.email)
            setCity(rowClicked.city)
            setCountry(rowClicked.country)
            setTimeUpdated(rowClicked.timeUpdated)
        }
        // getData('http://localhost:8000/customers')
        //   .then((data) => {
        //     console.log(data.customers)
        //     // setSumProducts(data.products.length)
        //     // setNoPage(Math.ceil(data.products.length / limit));
        //     //setProducts(data.products.slice((page - 1) * limit, page * limit))
        //     setCustomers(data.customers)
        //   })
        //   .catch((error) => {
        //     console.log(error)
        //   })
      }, [rowClicked])
    return(
        <>
            <Modal
                open={openModalEditCustomer}
                onClose={handleCancelModalClick}
                aria-labelledby="modal-modal-info"
                aria-describedby="modal-modal-info"
            >
                <Box sx={style} style={{ backgroundColor: 'white' }}>
                    <Typography mb={2} id="modal-modal-info" variant="h5" component="h2">
                        <strong>Sửa khách hàng</strong>
                    </Typography>
                    <br></br>
                    <Row>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Tên:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input defaultValue={nameCustomer} onChange={onNameCustomerChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Số điện thoại:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input defaultValue={phoneNumber} onChange={onPhoneNumberChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Email:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input defaultValue={email} type="text" readOnly/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Địa chỉ:</label>
                                        </Col>
                                        <Col sm="8">
                                            <textarea defaultValue={address} onChange={onAddressChange} style={{ width: '85%' }}></textarea>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="6">
                                    <Row>
                                        <Col sm="12">
                                            <Row>
                                                <Col sm="4">
                                                    <label>Thành phố:</label>
                                                </Col>
                                                <Col sm="8">
                                                    <Input defaultValue={city} type="text" onChange={onCityChange} />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col sm="6">
                                    <Row>
                                        <Col sm="12">
                                            <Row>
                                                <Col sm="4">
                                                    <label>Quốc gia:</label>
                                                </Col>
                                                <Col sm="8">
                                                    <Input defaultValue={country} type="text" onChange={onCountryChange} />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="2">
                                            <label>Ngày cập nhật:</label>
                                        </Col>
                                        <Col sm="10">
                                            <Input defaultValue={timeUpdated !== "" ? dtfUS.format(new Date(timeUpdated)) : ""} type="text" readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4 mb-4">
                                <Col sm="6">
                                    <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                                        Hủy Bỏ
                                    </Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onEditCustomerClick} className="bg-success w-75 text-white">
                                        Sửa Khách Hàng
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalEditCustomer