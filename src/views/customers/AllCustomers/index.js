import React, { useState, useEffect } from 'react'
import {
  Button,
  Container,
  Grid,
  Paper,
  Table,
  styled,
  tableCellClasses,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
  Input,
  FormControl,
  Select,
  MenuItem,
} from '@mui/material'
import ModalAddCustomer from './ModalAddCustomer'
import ModalEditCustomer from './ModalEditCustomer'
import { Col, Row } from 'reactstrap'
const dtfUS = new Intl.DateTimeFormat('uk', { day: '2-digit', month: '2-digit', year: 'numeric' })
const AllCustomers = () => {
  const style2 = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    p: 4,
    borderRadius: '10px',
  }
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }))
  // hàm gọi api
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }))
  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(1);
  const limit = 2;

  const [customers, setCustomers] = useState(null)
  const [varRefeshPage, setVarRefeshPage] = useState(0)
  const [openModalAddCustomer, setOpenModalAddCustomer] = useState(false);
  const [openModalEditCustomer, setOpenModalEditCustomer] = useState(false);
  const [rowClicked, setRowClicked] = useState(null)
  const [fullNameFilter, setFullNameFilter] = useState("")
  const [phoneNumberFilter, setPhoneNumberFilter] = useState("")
  const [emailFilter, setEmailFilter] = useState("")
  const [runOnFilter, setRunOnFilter] = useState(0)
  const [sumCustomer, setSumCustomer] = useState(0)
  const onBtnAddCustomerClick = () => {
    setOpenModalAddCustomer(true);
  }
  const onCloseModalAdd = () => {
    setOpenModalAddCustomer(false)
  }
  const onCloseModalEdit = () => {
    setOpenModalEditCustomer(false)
  }
  const onBtnEditClick = (row) => {
    setOpenModalEditCustomer(true);
    setRowClicked(row)
  }
  const onBtnFilterClick = () => {
    setRunOnFilter(runOnFilter + 1)
  }
  const onInputNameCustomerChange = (event) => {
    setFullNameFilter(event.target.value)
    setPage(1)
  }
  const onInputPhoneNumberChange = (event) => {
    setPhoneNumberFilter(event.target.value)
    setPage(1)
  }
  const onInputEmailChange = (event) => {
    setEmailFilter(event.target.value)
    setPage(1)
  }
  const changeHandlerComponent = (event, value) => {
    setPage(value);
  }
  useEffect(() => {
    getData('https://shop24h-backend.herokuapp.com/customers' + "?fullName=" + fullNameFilter + "&phoneNumber=" + phoneNumberFilter + "&email=" + emailFilter)
      .then((data) => {
        console.log(data)
        setSumCustomer(data.customers.length)
        setNoPage(Math.ceil(data.customers.length / limit));
        setCustomers(data.customers.slice((page - 1) * limit, page * limit))
      })
      .catch((error) => {
        console.log(error)
      })
  }, [varRefeshPage, runOnFilter, page])
  return (
    <>
    {/* <Row className='mb-2'>
        <Col sm="12">
          <Row>
            <Col sm="6">
              <small>
                Tổng sản phẩm: <strong>{0}</strong>
              </small>
            </Col>
            <Col sm="6" style={{ display: "flex" }}>
              <div style={{ marginLeft: "auto" }}>
                <Grid item xs={12} md={12} sm={12} lg={12} >
                  <Pagination onChange={changeHandlerComponent} count={noPage}></Pagination>
                </Grid>
              </div>
            </Col>
          </Row>
        </Col>
      </Row> */}
      <Container>
      <h2 className='text-center'>Tất cả khách hàng</h2>
      <Row className='mt-3'>
        <Col sm="12">
          <Row>
            <Col sm="1">
              <label>Tên: </label>
            </Col>
            <Col sm="2">
              <Input onChange={onInputNameCustomerChange}/>
            </Col>
            <Col sm="2">
              <label>Số điện thoại: </label>
            </Col>
            <Col sm="2">
              <Input onChange={onInputPhoneNumberChange}/>
            </Col>
            <Col sm="1">
              <label>Email: </label>
            </Col>
            <Col sm="2">
              <Input onChange={onInputEmailChange}/>
            </Col>
            <Col sm="2">
              <Button onClick={onBtnFilterClick} className='btn bg-success text-white w-100'>Lọc</Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <Button
        onClick={onBtnAddCustomerClick}
        className="mb-2 mt-3 "
        value="add-user"
        style={{
          borderRadius: 10,
          backgroundColor: '#689f38',
          padding: '10px 20px',
          fontSize: '10px',
        }}
        variant="contained"
      >
        Thêm +
      </Button>
      <br></br>
      <Row className='mb-2'>
        <Col sm="12">
          <Row>
            <Col sm="6">
              <small>
                Tìm thấy <strong>{sumCustomer}</strong> khách hàng
              </small>
            </Col>
            <Col sm="6" style={{ display: "flex" }}>
              <div style={{ marginLeft: "auto" }}>
                <Grid item xs={12} md={12} sm={12} lg={12} >
                  <Pagination onChange={changeHandlerComponent} count={noPage}></Pagination>
                </Grid>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Grid container>
        <Grid item xs={12} md={12} lg={12}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="users table">
              <TableHead>
                <TableRow style={{ backgroundColor: '#dcedc8' }}>
                  <TableCell align="center">STT</TableCell>
                  <TableCell align="center">ID</TableCell>
                  <TableCell align="center">Tên</TableCell>
                  <TableCell align="center">Số điện thoại</TableCell>
                  <TableCell align="center">Email</TableCell>
                  <TableCell align="center">Thao tác</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {customers
                  ? customers.map((row, index) => (
                    <StyledTableRow
                      key={index}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <StyledTableCell align="center">{index + 1}</StyledTableCell>
                      <StyledTableCell align="center">{row._id}</StyledTableCell>
                      <StyledTableCell align="center">{row.fullName}</StyledTableCell>
                      <StyledTableCell align="center">{row.phoneNumber}</StyledTableCell>
                      <StyledTableCell align="center">{row.email}</StyledTableCell>
                      <StyledTableCell align="center">
                        <Button
                          value={index}
                          onClick={() => {
                            onBtnEditClick(row)
                          }}
                          style={{
                            borderRadius: 25,
                            backgroundColor: '#2196f3',
                            padding: '10px 20px',
                            fontSize: '10px',
                          }}
                          variant="contained"
                        >
                          Sửa
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
                  : null}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <ModalAddCustomer getData={getData} setVarRefeshPage={setVarRefeshPage} varRefeshPage={varRefeshPage}  openModalAddCustomer={openModalAddCustomer} onCloseModalAdd={onCloseModalAdd}/>
      <ModalEditCustomer setRowClicked={setRowClicked} rowClicked={rowClicked} getData={getData} setVarRefeshPage={setVarRefeshPage} varRefeshPage={varRefeshPage}  openModalEditCustomer={openModalEditCustomer} onCloseModalEdit={onCloseModalEdit}/>
      </Container>
    </>
  )
}
export default AllCustomers
