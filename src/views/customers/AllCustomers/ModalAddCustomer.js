import {
    Alert,
    Box,
    Button,
    Input,
    MenuItem,
    Modal,
    Select,
    Snackbar,
    Typography,
} from '@mui/material'
import { useState } from 'react'
import { Col, Row } from 'reactstrap'
function ModalAddCustomer({ openModalAddCustomer, onCloseModalAdd, setVarRefeshPage, varRefeshPage, getData }) {

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        p: 4,
        borderRadius: '10px',
    }
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState('')
    const [statusModal, setStatusModal] = useState('error')
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const [nameCustomer, setNameCustomer] = useState("")
    const [phoneNumber, setphoneNumber] = useState("")
    const [email, setEmail] = useState("")
    const [address, setAddress] = useState("")
    const [city, setCity] = useState("")
    const [country, setCountry] = useState("")
    const handleCancelModalClick = () => {
        onCloseModalAdd()
        setNameCustomer("")
        setphoneNumber("")
        setEmail("")
        setAddress("")
        setCity("")
        setCountry("")
    }
    const onNameCustomerChange = (event) => {
        setNameCustomer(event.target.value)
    }
    const onPhoneNumberChange = (event) => {
        setphoneNumber(event.target.value)
    }
    const onEmailChange = (event) => {
        setEmail(event.target.value)
    }
    const onAddressChange = (event) => {
        setAddress(event.target.value)
    }
    const onCityChange = (event) => {
        setCity(event.target.value)
    }
    const onCountryChange = (event) => {
        setCountry(event.target.value)
    }
    const onAddCustomerClick = () => {
        console.log("nameCustomer", nameCustomer)
        console.log("phoneNumber", phoneNumber)
        console.log("email", email)
        console.log("address", address)
        console.log("city", city)
        console.log("country", country)
        var checKForm = validateForm()
        if(checKForm){
            getData("https://shop24h-backend.herokuapp.com/customers/" + email)
            .then((data) => {
                console.log(data.customer)
                if(data.customer === null){
                    getApiCreateCustomer()
                }
                else{
                    setOpenAlert(true)
                    setNoidungAlertValid('Trùng email!')
                    setStatusModal('error')
                    checKForm = false;
                }
            })
            .catch((error) => {
                console.log(error)
                setOpenAlert(true)
                setNoidungAlertValid('Lỗi mạng, xin thử lại!')
                setStatusModal('error')
            })
        }
        
    }
    const getApiCreateCustomer = () => {
        var infoCustomer = {
            fullName: nameCustomer,
            phoneNumber: phoneNumber,
            email: email,
            address: address,
            city: city,
            country: country
          }
          const body = {
            method: 'POST',
            body: JSON.stringify(infoCustomer),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          }
          getData('https://shop24h-backend.herokuapp.com/customers', body)
            .then((data) => {
              console.log(data)
              handleCancelModalClick()
              setOpenAlert(true)
              setNoidungAlertValid('Thêm thành công!')
              setStatusModal('success')
              setVarRefeshPage(varRefeshPage + 1)
            })
            .catch((error) => {
              console.log(error)
              setOpenAlert(true)
              setNoidungAlertValid('Lỗi thêm mới! Hãy thử lại!')
              setStatusModal('error')
            })
    }
    const validateForm = () => {
        const filterEmail = /^([a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)$/;
        const filterPhone = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
        const filterPhone2 = /(([02]))+([0-9]{9})\b/
        if(nameCustomer === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Tên khách hàng cần được nhập')
            setStatusModal('error')
            return false
        }
        if(!(filterPhone.test(phoneNumber) || filterPhone2.test(phoneNumber))){
            setOpenAlert(true)
            setNoidungAlertValid('Sai định dạng số điện thoại')
            setStatusModal('error')
            return false
        }
        if(!(filterEmail.test(email))){
            setOpenAlert(true)
            setNoidungAlertValid('Sai định dạng email')
            setStatusModal('error')
            return false
        }
        if(address === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Địa chỉ cần được nhập')
            setStatusModal('error')
            return false
        }
        if(city === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Thành phố cần được nhập')
            setStatusModal('error')
            return false
        }
        if(country === ""){
            setOpenAlert(true)
            setNoidungAlertValid('Quốc gia cần được nhập')
            setStatusModal('error')
            return false
        }
        else{
            return true;
        }
    }
    return (
        <>
            <Modal
                open={openModalAddCustomer}
                onClose={handleCancelModalClick}
                aria-labelledby="modal-modal-info"
                aria-describedby="modal-modal-info"
            >
                <Box sx={style} style={{ backgroundColor: 'white' }}>
                    <Typography mb={2} id="modal-modal-info" variant="h5" component="h2">
                        <strong>Thêm khách hàng</strong>
                    </Typography>
                    <br></br>
                    <Row>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Tên:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onNameCustomerChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Số điện thoại:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onPhoneNumberChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Email:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input type="text" onChange={onEmailChange}/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Địa chỉ:</label>
                                        </Col>
                                        <Col sm="8">
                                            <textarea onChange={onAddressChange} style={{ width: '85%' }}></textarea>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="6">
                                    <Row>
                                        <Col sm="12">
                                            <Row>
                                                <Col sm="4">
                                                    <label>Thành phố:</label>
                                                </Col>
                                                <Col sm="8">
                                                    <Input type="text" onChange={onCityChange} />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col sm="6">
                                    <Row>
                                        <Col sm="12">
                                            <Row>
                                                <Col sm="4">
                                                    <label>Quốc gia:</label>
                                                </Col>
                                                <Col sm="8">
                                                    <Input type="text" onChange={onCountryChange} />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    {/* <Row className="mt-2">
                        <Col sm="12">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="2">
                                            <label>Ngày tạo:</label>
                                        </Col>
                                        <Col sm="10">
                                            <Input type="text" readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row> */}
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4 mb-4">
                                <Col sm="6">
                                    <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                                        Hủy Bỏ
                                    </Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onAddCustomerClick} className="bg-success w-75 text-white">
                                        Thêm Khách Hàng
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalAddCustomer