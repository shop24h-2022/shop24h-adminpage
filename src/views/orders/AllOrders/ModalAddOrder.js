
import {
    Alert,
    Box,
    Button,
    FormControl,
    Input,
    MenuItem,
    Modal,
    Select,
    Snackbar,
    Typography,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { Col, Row } from 'reactstrap'
var arrayProductSelected = []
function ModalAddOrder({ onCloseModalAddOrder, setVarRefeshPage, varRefeshPage, getData, openModalAddOrder }) {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        p: 4,
        borderRadius: '10px',
    }
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState('')
    const [statusModal, setStatusModal] = useState('error')
    
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const [idCutomerForm, setIdCutomerForm] = useState("")
    const [selectedElementForm, setSelectedElementForm] = useState("none")
    const [amountForm, setAmountForm] = useState(0)
    const [productSelect, setProductSelect] = useState(null)
    const [arrayProductState, setArrayProductState] = useState(null)
    const [defaultValueSelect, setDefaultValueSelect] = useState("none")
    const [testState, setTestState] = useState(0) // biến tạm dùng để render
    const handleCancelModalClick = () => {
        onCloseModalAddOrder()
        arrayProductSelected = []
        setIdCutomerForm("")
        setSelectedElementForm("none")
        setAmountForm(0)
        setArrayProductState(null)
    }
    const onIdCustomerChange = (event) => {
        setIdCutomerForm(event.target.value)
    }
    const onSelectProductChange = (event) => {
        setSelectedElementForm(event.target.value)
        setDefaultValueSelect(event.target.value)
    }
    const onAmountProductChange = (event) => {
        setAmountForm(event.target.value)
    }
    const onAddProductToCreateOrderClick = () => {
        var checkUndefine = arrayProductSelected.find(e => e.id == selectedElementForm)
        var vCheckFrom = validateForm(checkUndefine);
        var objProductSelected = {
            id: selectedElementForm,
            amount: amountForm
        }
        console.log(checkUndefine)
        if(vCheckFrom){
            arrayProductSelected.push(objProductSelected)
            setArrayProductState(arrayProductSelected)
            setDefaultValueSelect("none")
            document.getElementById("inputAmount").value = ""
            setSelectedElementForm("none")
            setAmountForm(0)
        }
        console.log(arrayProductSelected)
    }
    const validateForm = (checkUndefine) => {
        if(selectedElementForm === "none"){
            setOpenAlert(true)
            setNoidungAlertValid('Bạn chưa chọn sản phẩm')
            setStatusModal('error')
            return false
        }
        if(amountForm <= 0 ){
            setOpenAlert(true)
            setNoidungAlertValid('Số lượng phải lớn hơn 0')
            setStatusModal('error')
            return false
        }
        if(checkUndefine != undefined){
            setOpenAlert(true)
            setNoidungAlertValid('Sản phẩm này có rồi')
            setStatusModal('error')
            setDefaultValueSelect("none")
            document.getElementById("inputAmount").value = ""
            setSelectedElementForm("none")
            return false
        }
        else{
            return true
        }
    }
    const onDeleteProductClick = (index) => {
        arrayProductSelected.splice(index, 1)
        setArrayProductState(arrayProductSelected)
        console.log(arrayProductSelected)
        setTestState(testState + 1)
    }
    const onCreateOrderClick = () => {
        var check = validateIdCusTomer()
        if(check){
            getCustomerByID()
        }
    }
    const validateIdCusTomer = () => {
        if(idCutomerForm == "") {
            setOpenAlert(true)
            setNoidungAlertValid('Bạn cần nhập Id khách hàng')
            setStatusModal('error')
            return false
        }
        else{
            return true
        }
    }
    const getCustomerByID = () => {
        console.log(idCutomerForm)
        getData('https://shop24h-backend.herokuapp.com/customers/' + idCutomerForm + "/getById")
            .then((data) => {
                console.log("Chạy rồi nè:", data)
                if(data.message == "Success"){
                    if(arrayProductSelected){
                        if(arrayProductSelected.length > 0){
                            getAPIPostOrder()
                        }
                        else{
                            setOpenAlert(true)
                            setNoidungAlertValid('Không có sản phẩm để tạo')
                            setStatusModal('error')
                        }
                    }
                    else{
                        setOpenAlert(true)
                        setNoidungAlertValid('Không có sản phẩm để tạo')
                        setStatusModal('error')
                    }
                }
                else{
                    setOpenAlert(true)
                    setNoidungAlertValid('Id của khách hàng không đúng')
                    setStatusModal('error')
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    const getAPIPostOrder = () => {
        var infoCreateOrder = {
            note: "Đơn hàng có " + arrayProductSelected.length + " sản phẩm."
        }
        const body = {
            method: 'POST',
            body: JSON.stringify(infoCreateOrder),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("https://shop24h-backend.herokuapp.com/customers/" + idCutomerForm + "/orders", body)
            .then((data) => {
                console.log(data)
                postAllOrderDetail(data)
                handleCancelModalClick()
                setOpenAlert(true)
                setNoidungAlertValid('Tạo đơn hàng thành công!')
                setStatusModal('success')
            })
            .catch((error) => {
                console.log(error)
                setOpenAlert(true)
                setNoidungAlertValid('Tạo đơn hàng thất bại, có thể do mạng!')
                setStatusModal('error')
            })
    }
    const postAllOrderDetail = (data) => {
        for (let i = 0; i < arrayProductState.length; i++) {
            getApiPostDetailOrder(arrayProductState[i], data)
        }
        setTimeout(function() {
            setVarRefeshPage(varRefeshPage + 1);
          }, 3000);
        
        
    }
    const getApiPostDetailOrder = (productChecked, data) => {
        var infoOrderDetail = {
            quantity: productChecked.amount
        }
        const body = {
            method: 'POST',
            body: JSON.stringify(infoOrderDetail),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("https://shop24h-backend.herokuapp.com/orders/" + data.order._id + "/" + productChecked.id + "/orderDetails", body)
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                console.log(error)
            })
    }
    useEffect(() => {
        getData('https://shop24h-backend.herokuapp.com/products')
            .then((data) => {
                console.log(data.products)
                setProductSelect(data.products)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [])
    return (
        <>
            <Modal
                open={openModalAddOrder}
                onClose={handleCancelModalClick}
                aria-labelledby="modal-modal-info"
                aria-describedby="modal-modal-info"
            >
                <Box sx={style} style={{ backgroundColor: 'white' }}>
                    <Typography mb={1} id="modal-modal-info" variant="h5" component="h2">
                        <strong>Thêm order</strong>
                    </Typography>
                    <br></br>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="2">
                                            <label>ID khách hàng:</label>
                                        </Col>
                                        <Col sm="10">
                                            <Input style={{ width: "94%" }} onChange={onIdCustomerChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className='mt-4'>
                        <Col sm="12">
                            <Row>
                                <Col sm="2">
                                    <label>Sản phẩm:</label>
                                </Col>
                                <Col sm="3">
                                    <FormControl sx={{ minWidth: 180 }}>
                                        <Select
                                            onChange={onSelectProductChange}
                                            id="products-select"
                                            
                                            value={defaultValueSelect ? defaultValueSelect : ""}
                                            style={{height: 30}}
                                        >
                                            <MenuItem value="none"> <em>Chọn sản phẩm</em> </MenuItem>
                                            {
                                            productSelect ? productSelect.map((row, index) =>
                                                <MenuItem value={row._id} key={index} name={row.name}>{row.name}</MenuItem>)
                                                : null
                                            }
                                        </Select>
                                    </FormControl>
                                </Col>
                                <Col sm="2">
                                    <label>Số lượng:</label>
                                </Col>
                                <Col sm="2">
                                    <Input id='inputAmount' onChange={onAmountProductChange} type='number' />
                                </Col>
                                <Col sm="3">
                                    <Button onClick={onAddProductToCreateOrderClick} className="bg-warning text-white">Thêm sản phẩm</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                            {arrayProductState
                              ? arrayProductState.map((row, index) => (
                                  <Col sm="6" key={index}>
                                    <i>id: {row.id}, SL: {row.amount}</i>
                                    <span

                                      onClick={() => {
                                        onDeleteProductClick(index)
                                      }}
                                      type="button"
                                      className="badge badge-warning ms-3" // ở đây dùng ms để cách dấu x ra
                                      id="lblCartCount"
                                    >
                                      {' '}
                                      x{' '}
                                    </span>
                                  </Col>
                                ))
                              : null}
                          </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4 mb-4">
                                <Col sm="6">
                                    <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                                        Hủy Bỏ
                                    </Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onCreateOrderClick} className="bg-success w-75 text-white">
                                        Tạo đơn hàng
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalAddOrder