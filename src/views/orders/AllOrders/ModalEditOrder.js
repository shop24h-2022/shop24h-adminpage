import {
    Alert,
    Box,
    Button,
    FormControl,
    Input,
    MenuItem,
    Modal,
    Select,
    Snackbar,
    Typography,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { Col, Row } from 'reactstrap'
function ModalEditOrder ({onCloseModalEditOrder, setVarRefeshPage, varRefeshPage, getData, openModalEditOrder, rowClicked}) {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 600,
        p: 4,
        borderRadius: '10px',
    }
    const handleCancelModalClick = () => {
        onCloseModalEditOrder()
    }
    const [defaultValueStatus, setDefaultValueStatus] = useState(-1)
    const [status, setStatus] = useState(-1)
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState('')
    const [statusModal, setStatusModal] = useState('error')
    
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onSelectStatusChange = (event) => {
        setStatus(event.target.value)
    }
    const onBtnEditOrderClick = () => {
        if(status == rowClicked.status){
            setOpenAlert(true)
            setNoidungAlertValid('Trạng thái không thay đổi')
            setStatusModal('error')
            return false
        }
        else{
            var vInfoStatus = {
                status: parseInt(status)
            }
            const body = {
                method: 'PUT',
                body: JSON.stringify(vInfoStatus),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData("https://shop24h-backend.herokuapp.com/orders/" + rowClicked._id, body)
                .then((data) => {
                    setStatusModal("success");
                    setNoidungAlertValid("Cập nhật thành công!");
                    setOpenAlert(true);
                    handleCancelModalClick();
                    setVarRefeshPage(varRefeshPage + 1);
                })
                .catch((error) => {
                    setStatusModal("error");
                    setNoidungAlertValid("Lỗi mạng, xin thử lại!");
                    setOpenAlert(true);
                    console.log(error)
                })
        }
    }
    useEffect(() => {
        if(rowClicked){
            setDefaultValueStatus(rowClicked.status)
        }
        
      }, [rowClicked])
    return (
        <>
            <Modal
                open={openModalEditOrder}
                onClose={handleCancelModalClick}
                aria-labelledby="modal-modal-info"
                aria-describedby="modal-modal-info"
            >
                <Box sx={style} style={{ backgroundColor: 'white' }}>
                    <Typography mb={1} id="modal-modal-info" variant="h5" component="h2">
                        <strong>Edit order</strong>
                    </Typography>
                    <br></br>
                    <Row >
                        <Col sm="12">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="5">
                                            <label>Trạng thái đơn hàng:</label>
                                        </Col>
                                        <Col sm="7">
                                        <FormControl sx={{ minWidth: 270 }}>
                                            <Select
                                                id="status-select"
                                                defaultValue={defaultValueStatus}
                                                onChange={onSelectStatusChange}
                                            >
                                                <MenuItem value="0">Đã tiếp nhận</MenuItem>
                                                <MenuItem value="1">Đang giao</MenuItem>
                                                <MenuItem value="2">Đã giao thành công</MenuItem>
                                                <MenuItem value="3">Đã hủy</MenuItem>
                                            </Select>
                                            </FormControl>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4 mb-4">
                                <Col sm="6">
                                    <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                                        Hủy Bỏ
                                    </Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnEditOrderClick}  className="bg-success w-75 text-white">
                                        Sửa Đơn Hàng
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalEditOrder