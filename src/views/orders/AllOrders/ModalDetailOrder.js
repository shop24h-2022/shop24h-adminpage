import { Snackbar, Alert, Box, Grid, Menu, MenuItem, Modal, Table, tableCellClasses, TableBody, Paper, TableCell, styled, TableContainer, TableHead, TableRow, Typography, Button, Input } from "@mui/material";
import { useEffect, useState } from 'react';
import { Col, Row } from "reactstrap";
function ModalDetailOrder ({openModalDetailOrder, onCloseModalDetailOrderClick, orderIdModal, orderDetailModal, arrayProductState}) {
    const style2 = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 1000,
        p: 4,
        borderRadius: '10px'
    };
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));
    const onCloseDetailOrderClick = ()=> {
        onCloseModalDetailOrderClick()
    }
    return(
        <>
            <Modal
                open={openModalDetailOrder}
                onClose={onCloseDetailOrderClick}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-confirm"
                style={{borderRadius: 10}}
            >
                <Box sx={style2} style={{ backgroundColor: "white" }}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Chi tiết mã đơn hàng {orderIdModal}</strong><br></br>
                    </Typography>
                    <br></br>
                    <Grid container>
                        <Grid item xs={12} md={12} lg={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="cart table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">STT</TableCell>
                                            <TableCell align="center">Sản phẩm</TableCell>
                                            <TableCell align="center">Tên sản phẩm</TableCell>
                                            <TableCell align="center">Đơn giá</TableCell>
                                            <TableCell align="center">Số lượng sản phẩm</TableCell>
                                            <TableCell align="center">Số Tiền</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {orderDetailModal ? orderDetailModal.map((row, index) => (
                                            <StyledTableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                                <StyledTableCell align="center">{index + 1}</StyledTableCell>
                                                <StyledTableCell align="center"><img src={arrayProductState ? arrayProductState[index].imageUrl[0] : ""} style={{ height: 40, width: 40 }}></img></StyledTableCell>
                                                <StyledTableCell align="center">{arrayProductState ? arrayProductState[index].name : ""}</StyledTableCell>
                                                <StyledTableCell align="center">{(row.priceEach).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} vnđ</StyledTableCell>
                                                <StyledTableCell align="center">{row.quantity}</StyledTableCell>
                                                
                                                <StyledTableCell align="center">{(row.quantity*row.priceEach).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} vnđ</StyledTableCell>
                                            </StyledTableRow >
                                        ))
                                            :
                                            null
                                        }

                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                    <Row className="mt-2">
                        <Col sm="12">
                            {/* <p>Tổng: <strong style={{ color: "#ee4d2d", fontSize: 30 }}>{sumMoney.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</strong></p> */}
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-2">
                                <Col sm="12">
                                    <Button onClick={onCloseDetailOrderClick} style={{ backgroundColor: "#ee4d2d" }} className="w-100 text-white">Đóng</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
        </>
    )
}

export default ModalDetailOrder