import React from 'react'
import {
  Button,
  Container,
  Grid,
  Paper,
  Table,
  styled,
  tableCellClasses,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
  Input,
  FormControl,
  Select,
  MenuItem,
} from '@mui/material'
import { useEffect, useState } from 'react'
import { Col, Label, Row } from 'reactstrap'
import ModalAddOrder from "./ModalAddOrder"
import ModalEditOrder from "./ModalEditOrder"
import ModalDetailOrder from './ModalDetailOrder'
const dtfUS = new Intl.DateTimeFormat('uk', { day: '2-digit', month: '2-digit', year: 'numeric' })
const AllOrders = () => {
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }))

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }))
  // hàm gọi api
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(1);
  const limit = 4;
  const [openModalAddOrder, setOpenModalAddOrder] = useState(false);
  const [openModalEditOrder, setOpenModalEditOrder] = useState(false);
  const [varRefeshPage, setVarRefeshPage] = useState(0)
  const [orders, setOrders] = useState(null)
  const [sumOrder, setSumOrder] = useState(0);
  const [runOnFilter, setRunOnFilter] = useState(0)
  const [valueSelectFilter, setValueSelectFilter] = useState(-1)
  const [customerIdFilter, setCustomerIdFilter] = useState("")
  const [rowClicked, setRowClicked] = useState(null)
  const [openModalDetailOrder, setOpenModalDetailOrder] = useState(false)
  const [orderIdModal, setOrderIdModal] = useState("")
  const [orderDetailModal, setOrderDetailModal] = useState(null)
  const [arrayProductState, setArrayProductState] = useState(null)
  const changeHandlerComponent = (event, value) => {
    setPage(value);
  }
  const onBtnAddOrderClick = () => {
    setOpenModalAddOrder(true);
  }
  const onBtnEditOrderClick = (row) => {
    setOpenModalEditOrder(true);
    console.log(row)
    setRowClicked(row)
  }
  const onCloseModalAddOrder = () => {
    setOpenModalAddOrder(false);
  }
  const onCloseModalDetailOrderClick = () => {
    setOpenModalDetailOrder(false)
    setArrayProductState(null);
}
  const onCloseModalEditOrder = () => {
    setOpenModalEditOrder(false);
  }
  const onSelectStatusChange = (event) => {
    console.log(parseInt(event.target.value))
    setValueSelectFilter(parseInt(event.target.value))
  }
  const onCustomerIdFilterChange = (event) => {
    setCustomerIdFilter(event.target.value)
  }
  const onBtnFilterClick = () => {
    setRunOnFilter(runOnFilter + 1)
    setPage(1)
  }
  const onDetailClick = (paramOrderId) => {
    console.log(paramOrderId)
    setOrderIdModal(paramOrderId)
    setOpenModalDetailOrder(true)
    getData("https://shop24h-backend.herokuapp.com/orders/" + paramOrderId + "/orderDetails")
        .then((data) => {
            console.log(data)
            setOrderDetailModal(data.Order.orderDetails)
            var orderDetail = data.Order.orderDetails
            console.log(orderDetail)
            var arrayProduct = [];
            for(let i = 0; i< orderDetail.length; i++){
                callApiGetInfoProducts(orderDetail[i].product, arrayProduct, orderDetail.length)
            }
        })
        .catch((error) => {
            console.log(error)
        })
  }
  const callApiGetInfoProducts = (paramProductId, arrayProduct, lengthOrderDetail) => {
    getData("https://shop24h-backend.herokuapp.com/products/" + paramProductId)
        .then((data) => {
            arrayProduct.push(data.product)
            if(arrayProduct.length == lengthOrderDetail){
                console.log(arrayProduct)
                setArrayProductState(arrayProduct)
            }
        })
        .catch((error) => {
            console.log(error)
        })
  }
  useEffect(() => {
    getData('https://shop24h-backend.herokuapp.com/orders' + "?customerId=" + customerIdFilter + "&status=" + valueSelectFilter)
      .then((data) => {
        console.log(data)
        setSumOrder(data.order.length)
        setNoPage(Math.ceil(data.order.length / limit));
        setOrders(data.order.slice((page - 1) * limit, page * limit))
      })
      .catch((error) => {
        console.log(error)
      })
  }, [varRefeshPage, runOnFilter, page])
  return (
    <>
      <Container>
      <h2 className='text-center'>Tất cả đơn hàng</h2>
      <Row className='mt-3'>
        <Col sm="5">
          <Row>
            <Col sm="12">
              <Row>
                <Col sm="4">
                  <Label>
                    <strong>ID khách hàng:</strong>
                  </Label>
                </Col>
                <Col sm="8">
                  <Input onChange={onCustomerIdFilterChange}/>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col sm="5">
          <Row>
            <Col sm="12">
              <Row>
                <Col sm="4">
                  <Label>
                    <strong>Trạng thái</strong>
                  </Label>
                </Col>
                <Col sm="8">
                  <FormControl sx={{ minWidth: 180 }}>
                      <Select
                        id="status-select"
                        defaultValue="none"
                        onChange={onSelectStatusChange}
                      >
                        <MenuItem value="none"> <em>Tất cả đơn hàng</em> </MenuItem>
                        <MenuItem value="0">Đã tiếp nhận</MenuItem>
                        <MenuItem value="1">Đang giao</MenuItem>
                        <MenuItem value="2">Đã giao thành công</MenuItem>
                        <MenuItem value="3">Đã hủy</MenuItem>
                      </Select>
                    </FormControl>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        
        <Col sm="2">
          <Button onClick={onBtnFilterClick} className='btn bg-success text-white w-100'>Lọc</Button>
        </Col>
      </Row>
      <Button
        className="mb-2 mt-3 "
        value="add-order"
        style={{
          borderRadius: 10,
          backgroundColor: '#689f38',
          padding: '10px 20px',
          fontSize: '10px',
        }}
        variant="contained"
        onClick={onBtnAddOrderClick}
      >
        Thêm +
      </Button>
      <br></br>
      <Row className='mb-2'>
        <Col sm="12">
          <Row>
            <Col sm="6">
              <small>
                Tìm thấy <strong>{sumOrder}</strong> đơn hàng
              </small>
            </Col>
            <Col sm="6" style={{ display: "flex" }}>
              <div style={{ marginLeft: "auto" }}>
                <Grid item xs={12} md={12} sm={12} lg={12} >
                  <Pagination onChange={changeHandlerComponent} count={noPage}></Pagination>
                </Grid>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>

      <Grid container >
        <Grid item xs={12} md={12} lg={12}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="orders table">
              <TableHead>
                <TableRow style={{ backgroundColor: '#dcedc8' }}>
                  <TableCell align="center">STT</TableCell>
                  <TableCell align="center">ID</TableCell>
                  <TableCell align="center">ID khách hàng</TableCell>
                  <TableCell align="center">Ghi chú</TableCell>
                  <TableCell align="center">Trạng thái</TableCell>
                  <TableCell align="center">Ngày update</TableCell>
                  <TableCell align="center">Thao tác</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {orders
                  ? orders.map((row, index) => (
                    <StyledTableRow
                      key={index}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <StyledTableCell align="center">{index + 1}</StyledTableCell>
                      <StyledTableCell align="center">{row._id}</StyledTableCell>
                      <StyledTableCell align="center">{row.customer}</StyledTableCell>
                      <StyledTableCell align="center">{row.note}</StyledTableCell>
                      <StyledTableCell align="center">{row.status == 0 ? "Đã tiếp nhận" : row.status == 1 ? "Đang giao" : row.status == 2 ? "Đã giao thành công" : "Đã hủy"}</StyledTableCell>
                      <StyledTableCell align="center">
                        {dtfUS.format(new Date(row.timeUpdated))}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        <Button
                          value={index}
                          onClick={() => {
                            onBtnEditOrderClick(row)
                          }}
                          style={{
                            borderRadius: 25,
                            backgroundColor: '#2196f3',
                            padding: '10px 20px',
                            fontSize: '10px',
                          }}
                          variant="contained"
                        >
                          Sửa
                        </Button>
                        <Button
                          value={index * index}
                          onClick={() => {
                            onDetailClick(row._id)
                          }}
                          style={{
                            borderRadius: 25,
                            backgroundColor: '#f50057',
                            padding: '10px 20px',
                            fontSize: '10px',
                          }}
                          variant="contained"
                        >
                          Chi tiết
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
                  : null}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    <ModalAddOrder onCloseModalAddOrder={onCloseModalAddOrder} setVarRefeshPage={setVarRefeshPage} varRefeshPage={varRefeshPage} getData={getData} openModalAddOrder={openModalAddOrder}/>
    <ModalEditOrder rowClicked={rowClicked} onCloseModalEditOrder={onCloseModalEditOrder} setVarRefeshPage={setVarRefeshPage} varRefeshPage={varRefeshPage} getData={getData} openModalEditOrder={openModalEditOrder}/>
    <ModalDetailOrder arrayProductState={arrayProductState} orderDetailModal={orderDetailModal} orderIdModal={orderIdModal} openModalDetailOrder={openModalDetailOrder} onCloseModalDetailOrderClick={onCloseModalDetailOrderClick}/>
    </Container>
    </>
  )
}
export default AllOrders
