
import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material"
import { useState } from "react"
import { Col, Row } from "reactstrap"

function ModalDeleteProduct ({varRefeshPage, setVarRefeshPage, openModalDelete, rowClickedDelete, handleCloseDelete}) {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        p: 4,
        borderRadius: '10px',
      }
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState('error')

    const handleCloseAlert = () => {
    setOpenAlert(false)
    }
    const handleCancelModalClick = () => {
        handleCloseDelete()
    }
    const onDeleteProductClick = () => {
        getApiDeleteProduct()
    }
    const getApiDeleteProduct = () => {
        // gọi api delete order
        const vURL_DELETE = 'https://shop24h-backend.herokuapp.com/products/' + rowClickedDelete._id
        fetch(vURL_DELETE, { method: 'DELETE' })
            .then(async response => {
                const isJson = response.headers.get('content-type')?.includes('application/json');
                const data = isJson && await response.json();
                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }
                console.log(data)
                setVarRefeshPage(varRefeshPage + 1)
                setOpenAlert(true)
                setNoidungAlertValid('Xóa sản phẩm thành công!')
                setStatusModal('success')
                handleCancelModalClick()
            })
            .catch(error => {
                console.log(error)
                setOpenAlert(true)
                setNoidungAlertValid('xóa sản phẩm thất bại, xin thử lại')
                setStatusModal('error')
            });
    }
    return (
        <>
            <Modal
        open={openModalDelete}
        onClose={handleCancelModalClick}
        aria-labelledby="modal-modal-info"
        aria-describedby="modal-modal-info"
      >
        <Box sx={style} style={{ backgroundColor: 'white' }}>
          <Typography mb={1} id="modal-modal-info" variant="h5" component="h2">
            <strong>Xóa sản phẩm</strong>
          </Typography>
          <br></br>
            <Row>
            <Col sm="12">
              <Row>
                <p>Bạn có muốn xóa sản phẩm: </p>
                <small>ID: <strong>{rowClickedDelete ? rowClickedDelete._id : ""}</strong></small><br></br>
                <small>Name: <strong>{rowClickedDelete ? rowClickedDelete.name : ""}</strong></small>
              </Row>
            </Col>
        </Row>
          <Row className="mt-4 text-center">
            <Col sm="12">
              <Row className="mt-4 mb-4">
                <Col sm="6">
                  <Button onClick={handleCancelModalClick} className="bg-warning w-75 text-white">
                    Hủy Bỏ
                  </Button>
                </Col>
                <Col sm="6">
                  <Button onClick={onDeleteProductClick} className="bg-warning w-75 text-white">
                    Delete Product
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
        </>
    )
}

export default ModalDeleteProduct