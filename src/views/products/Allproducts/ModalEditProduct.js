import {
  Alert,
  Box,
  Button,
  Input,
  MenuItem,
  Modal,
  Select,
  Snackbar,
  Typography,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { Col, Row } from 'reactstrap'
var subArrayImg = []
function ModalEditProduct({
  varRefeshPage,
  openModalEdit,
  handleCloseEdit,
  getData,
  setVarRefeshPage,
  rowClicked,
  setRowClicked
}) {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    p: 4,
    borderRadius: '10px',
  }
  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('error')

  const [arrayImages, setArrayImages] = useState(null)
  const [arrayImagesEdit, setArrayImagesEdit] = useState(null)
  const [nameProductDefaulValue, setNameProductDefaulValue] = useState('')
  const [typeProductDefaulValue, setTypeProductDefaulValue] = useState('')
  const [buyPriceDefaulValue, setbuyPriceDefaulValue] = useState(0)
  const [promotionPriceDefaulValue, setPromotionPriceDefaulValue] = useState(0)
  const [descriptionDefaulValue, setDescriptionDefaulValue] = useState('')
  const [img, setImg] = useState('')

  const [testState, setTestState] = useState(0) // biến tạm dùng để render

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }
  const handleCancelModalClick = () => {
    handleCloseEdit()
    setVarRefeshPage(varRefeshPage + 1)
  }

  const onNameProductChange = (event) => {
    setNameProductDefaulValue(event.target.value)
  }
  const onTypeProductChange = (event) => {
    setTypeProductDefaulValue(event.target.value)
  }
  const onBuyPriceChange = (event) => {
    setbuyPriceDefaulValue(event.target.value !== "" ? parseInt(event.target.value) : 0)
  }
  const onPromotionPriceChange = (event) => {
    setPromotionPriceDefaulValue(event.target.value !== "" ? parseInt(event.target.value) : 0)
  }
  const onImgChange = (event) => {
    setImg(event.target.value)
  }
  const onDescriptionChange = (event) => {
    setDescriptionDefaulValue(event.target.value)
  }

  const onThemAnhClick = () => {
    if (subArrayImg.length == 4 && img !== '') {
      setOpenAlert(true)
      setNoidungAlertValid('Tối đa 4 ảnh')
      setStatusModal('error')
      document.getElementById('inputImgUrl').value = ''
      setImg('')
      return false
    }
    if (img !== '' && subArrayImg.includes(img) === false && subArrayImg.length < 5) {
      subArrayImg.push(img)
      setArrayImagesEdit(subArrayImg)
      setImg('')
      console.log(subArrayImg)
      document.getElementById('inputImgUrl').value = ''
      setImg('')
      return true
    }
    if (subArrayImg.includes(img)) {
      setOpenAlert(true)
      setNoidungAlertValid('Ảnh này có rồi')
      setStatusModal('error')
      document.getElementById('inputImgUrl').value = ''
      setImg('')
    }
  }

  const onDeleteImgClick = (index) => {
    console.log(arrayImages)
    console.log(index)
    subArrayImg.splice(index, 1)
    setArrayImagesEdit(subArrayImg)
    console.log(subArrayImg)
    setTestState(testState + 1)
  }
  const onEditProductClick = () => {
    console.log("nameProduct", nameProductDefaulValue);
    console.log("typeProduct", typeProductDefaulValue);
    console.log("buyPrice", buyPriceDefaulValue);
    console.log("promotionPrice", promotionPriceDefaulValue);
    console.log("arr", arrayImages);
    console.log("description", descriptionDefaulValue);
    console.log(rowClicked)
    var vCheck = validateForm();
    if(vCheck){
        getAllproduct()
    }
  }
  const getAllproduct = () => {
    getData('https://shop24h-backend.herokuapp.com/products')
      .then((data) => {
        console.log(data.products)
        var vCheckFor = true
        for (let i = 0; i < data.products.length; i++) {
          if ((data.products[i].name.toLowerCase() == nameProductDefaulValue.toLowerCase()) && (data.products[i]._id != rowClicked._id)) {
            setOpenAlert(true)
            setNoidungAlertValid('Trùng tên sản phẩm')
            setStatusModal('error')
            return (vCheckFor = false)
          }
        }
        if (vCheckFor) {
          getApiPostProduct()
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }
  const getApiPostProduct = () => {
    var infoProduct = {
      name: nameProductDefaulValue,
      type: typeProductDefaulValue,
      imageUrl: arrayImages,
      buyPrice: buyPriceDefaulValue,
      promotionPrice: promotionPriceDefaulValue,
      description: descriptionDefaulValue,
    }
    const body = {
      method: 'PUT',
      body: JSON.stringify(infoProduct),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    getData('https://shop24h-backend.herokuapp.com/products/' + rowClicked._id, body)
      .then((data) => {
        console.log(data)
        setOpenAlert(true)
        setNoidungAlertValid('Sửa sản phẩm thành công!')
        setStatusModal('success')
        setVarRefeshPage(varRefeshPage + 1)
        handleCancelModalClick()
      })
      .catch((error) => {
        console.log(error)
        setOpenAlert(true)
        setNoidungAlertValid('Lỗi sửa! Hãy thử lại!')
        setStatusModal('error')
      })
  }
  const validateForm = () => {
    if (nameProductDefaulValue === '') {
      setOpenAlert(true)
      setNoidungAlertValid('Tên sản phẩm cần được nhập')
      setStatusModal('error')
      return false
    }
    if (buyPriceDefaulValue <= 0) {
      setOpenAlert(true)
      setNoidungAlertValid('Giá mua không được âm hoặc bằng 0 hoặc không nhập ')
      setStatusModal('error')
      return false
    }
    if (promotionPriceDefaulValue <= 0) {
      setOpenAlert(true)
      setNoidungAlertValid('Giá ưu đãi không được âm hoặc bằng 0 hoặc không nhập ')
      setStatusModal('error')
      return false
    }
    if (arrayImages.length !== 3 && arrayImages.length !== 4) {
      setOpenAlert(true)
      setNoidungAlertValid('Số lượng ảnh là 3 hoặc 4')
      setStatusModal('error')
      return false
    }
    if (descriptionDefaulValue === '') {
      setOpenAlert(true)
      setNoidungAlertValid('Nên để mô tả để khách hàng hiểu rõ sản phẩm')
      setStatusModal('error')
      return false
    } 
    else {
      return true
    }
  }
  useEffect(() => {
    if (rowClicked) {
      setArrayImages(rowClicked.imageUrl)
      setArrayImagesEdit(rowClicked.imageUrl)
      setNameProductDefaulValue(rowClicked.name)
      setTypeProductDefaulValue(rowClicked.type)
      setbuyPriceDefaulValue(rowClicked.buyPrice)
      setPromotionPriceDefaulValue(rowClicked.promotionPrice)
      setDescriptionDefaulValue(rowClicked.description)
      subArrayImg = rowClicked.imageUrl
      console.log('render')
    }
  }, [rowClicked])
  return (
    <>
      <Modal
        open={openModalEdit}
        onClose={handleCancelModalClick}
        aria-labelledby="modal-modal-info"
        aria-describedby="modal-modal-info"
      >
        <Box sx={style} style={{ backgroundColor: 'white' }}>
          <Typography mb={2} id="modal-modal-info" variant="h5" component="h2">
            <strong>Sửa sản phẩm</strong>
          </Typography>
          <br></br>
          <Row>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Tên sản phẩm:</label>
                    </Col>
                    <Col sm="8">
                      <Input defaultValue={nameProductDefaulValue} onChange={onNameProductChange} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="5">
                      <label>Loại sản phẩm:</label>
                    </Col>
                    <Col sm="7">
                      <Select
                        id="type-product-select"
                        defaultValue={typeProductDefaulValue}
                        sx={{ minWidth: 168 }}
                        onChange={onTypeProductChange}
                      >
                        <MenuItem value="Fruits">Fruits</MenuItem>
                        <MenuItem value="Juice">Juice</MenuItem>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Giá mua:</label>
                    </Col>
                    <Col sm="8">
                      <Input
                        defaultValue={buyPriceDefaulValue}
                        type="number"
                        onChange={onBuyPriceChange}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Giá ưu đãi:</label>
                    </Col>
                    <Col sm="8">
                      <Input
                        defaultValue={promotionPriceDefaulValue}
                        type="number"
                        onChange={onPromotionPriceChange}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col sm="12">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="2">
                      <label>Image URL:</label>
                    </Col>
                    <Col sm="10">
                      <Row>
                        <Col sm="12">
                          <Row>
                            <Col sm="9">
                              <Input
                                id="inputImgUrl"
                                style={{ width: '94%' }}
                                onChange={onImgChange}
                                placeholder="nhập URL vào đây, 3 hoặc 4 ảnh"
                              />
                            </Col>
                            <Col sm="3">
                              <Button
                                className="bg-warning w-75 text-white"
                                onClick={onThemAnhClick}
                              >
                                Thêm ảnh
                              </Button>
                            </Col>
                          </Row>
                          <Row className="mt-3">
                            {arrayImages
                              ? arrayImages.map((row, index) => (
                                  <Col sm="3" key={index}>
                                    <img
                                      alt={index}
                                      src={row}
                                      style={{ height: 60, width: 60 }}
                                    ></img>
                                    <span
                                      onClick={() => {
                                        onDeleteImgClick(index)
                                      }}
                                      type="button"
                                      className="badge badge-warning"
                                      id="lblCartCount"
                                    >
                                      {' '}
                                      x{' '}
                                    </span>
                                  </Col>
                                ))
                              : null}
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col sm="12">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="2">
                      <label>Mô tả:</label>
                    </Col>
                    <Col sm="10">
                      <textarea
                        defaultValue={descriptionDefaulValue}
                        onChange={onDescriptionChange}
                        style={{ width: '94%' }}
                      ></textarea>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <small style={{ fontSize: 12 }}>
              Hãy kiểm tra ảnh có hiển thị không trước khi click Sửa Sản Phẩm nhé!
            </small>
          </Row>
          <Row className="mt-4 text-center">
            <Col sm="12">
              <Row className="mt-4 mb-4">
                <Col sm="6">
                  <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                    Hủy Bỏ
                  </Button>
                </Col>
                <Col sm="6">
                  <Button onClick={onEditProductClick} className="bg-success w-75 text-white">
                    Sửa Sản Phẩm
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}

export default ModalEditProduct
