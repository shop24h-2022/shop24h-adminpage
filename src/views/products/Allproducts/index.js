import {
  Button,
  Container,
  Grid,
  Paper,
  Table,
  styled,
  tableCellClasses,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
  Input,
  FormControl,
  Select,
  MenuItem,
} from '@mui/material'
import { useEffect, useState } from 'react'
import ModalAddProduct from './ModalAddProduct'
import ModalEditProduct from './ModalEditProduct'
import ModalDeleteProduct from "./ModalDeleteProduct"
import { Col, Label, Row } from 'reactstrap'
const dtfUS = new Intl.DateTimeFormat('uk', { day: '2-digit', month: '2-digit', year: 'numeric' })

function AllProducts() {
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }))

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }))

  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(1);
  const limit = 4;
  // hàm gọi api
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  const [products, setProducts] = useState(null)
  const [sumProducts, setSumProducts] = useState(0)
  const [varRefeshPage, setVarRefeshPage] = useState(0)
  const [openModalAdd, setOpenModalAdd] = useState(false)
  const [openModalDelete, setOpenModalDelete] = useState(false)
  const [openModalEdit, setOpenModalEdit] = useState(false)
  const [rowClicked, setRowClicked] = useState(null)
  const [rowClickedDelete, setRowClickedDelete] = useState(null)

  const [fillterName, setFillterName] = useState("");
  const [priceMin, setPriceMin] = useState(0);
  const [priceMax, setPriceMax] = useState(1000000);
  const [valueSelectTypeProduct, setValueSelectTypeProduct] = useState("none")
  const [runOnFilter, setRunOnFilter] = useState(0)

  const changeFillterNameInput = (event) => {
    setFillterName(event.target.value)
    setPage(1)
  }
  const changeFillterPriceMinInput = (event) => {
    setPriceMin(event.target.value)
    setPage(1)
  }
  const changeFillterPriceMaxInput = (event) => {
    if (event.target.value != "") {
      setPriceMax(event.target.value)
      setPage(1)
    }
    else {
      setPriceMax(1000000)
      setPage(1)
    }
  }
  const onSelectProductType = (event) => {
    setValueSelectTypeProduct(event.target.value)
    setPage(1)
    console.log(event.target.value)
  }
  const onBtnFilterClicked = () => {
    setRunOnFilter(runOnFilter + 1)
  }
  const handleClose = () => {
    setOpenModalAdd(false)
  }
  const handleCloseEdit = () => {
    setOpenModalEdit(false)
  }
  const handleCloseDelete = () => {
    setOpenModalDelete(false)
  }
  const onBtnAddProductClick = () => {
    setOpenModalAdd(true)
  }
  const onBtnEditClick = (row) => {
    setOpenModalEdit(true)
    setRowClicked(row)
    console.log(row)
  }
  const onBtnDeleteClick = (row) => {
    setOpenModalDelete(true)
    setRowClickedDelete(row)
  }
  const changeHandlerComponent = (event, value) => {
    setPage(value);
  }
  useEffect(() => {
    getData('https://shop24h-backend.herokuapp.com/products' + "?name=" + fillterName + "&priceMin=" + priceMin + "&priceMax=" + priceMax + "&valueSelected=" + valueSelectTypeProduct)
      .then((data) => {
        console.log(data.products)
        setSumProducts(data.products.length)
        setNoPage(Math.ceil(data.products.length / limit));
        setProducts(data.products.slice((page - 1) * limit, page * limit))
      })
      .catch((error) => {
        console.log(error)
      })
  }, [page, varRefeshPage, runOnFilter])
  return (
    <Container>
      <h2 className='text-center'>Tất cả sản phẩm</h2>
      <Row className='mt-3 text-center'>
        <Col sm="3">
          <Row>
            <Col sm="12">
              <Row>
                <Col sm="3">
                  <Label>
                    <strong>Tên:</strong>
                  </Label>
                </Col>
                <Col sm="9">
                  <Input onChange={changeFillterNameInput} style={{ width: 180 }} />
                </Col>
              </Row>
            </Col>

          </Row>
        </Col>
        <Col sm="4">
          <Row>
            <Col sm="12">
              <Row>
                <Col sm="4">
                  <Label>
                    <strong>Giá (VNĐ):</strong>
                  </Label>
                </Col>
                <Col sm="8">
                  <Row>
                    <Col sm="12">
                      <Row>
                        <Col sm="6">
                          <Input type="number" onChange={changeFillterPriceMinInput} className="w-100" placeholder="min" />
                        </Col>
                        <Col sm="6">
                          <Input type="number" onChange={changeFillterPriceMaxInput} className="w-100" placeholder="max" />
                        </Col>
                      </Row>
                    </Col>

                  </Row>
                </Col>
              </Row>
            </Col>

          </Row>
        </Col>

        <Col sm="3">
          <Row>
            <Col sm="12">
              <Row>
                <Col sm="4">
                  <Label>
                    <strong>Loại:</strong>
                  </Label>
                </Col>
                <Col sm="8">
                  <FormControl sx={{ minWidth: 180 }}>
                    <Select
                      id="products-select"
                      defaultValue="none"
                      onChange={onSelectProductType}
                    >
                      <MenuItem value="none"> <em>Tất cả sản phẩm</em> </MenuItem>
                      <MenuItem value="Juice">Juice</MenuItem>
                      <MenuItem value="Fruits">Fruits</MenuItem>
                    </Select>
                  </FormControl>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col sm="2">
          <Button className='btn bg-success text-white w-100' onClick={onBtnFilterClicked}>Lọc</Button>
        </Col>
      </Row>
      <Button
        onClick={onBtnAddProductClick}
        className="mb-2 mt-3 "
        value="add-product"
        style={{
          borderRadius: 10,
          backgroundColor: '#689f38',
          padding: '10px 20px',
          fontSize: '10px',
        }}
        variant="contained"
      >
        Thêm +
      </Button>
      <br></br>
      <Row className='mb-2'>
        <Col sm="12">
          <Row>
            <Col sm="6">
              <small>
                Tìm thấy <strong>{sumProducts}</strong> sản phẩm
              </small>
            </Col>
            <Col sm="6" style={{ display: "flex" }}>
              <div style={{ marginLeft: "auto" }}>
                <Grid item xs={12} md={12} sm={12} lg={12} >
                  <Pagination onChange={changeHandlerComponent} count={noPage}></Pagination>
                </Grid>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>

      <Grid container>
        <Grid item xs={12} md={12} lg={12}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="users table">
              <TableHead>
                <TableRow style={{ backgroundColor: '#dcedc8' }}>
                  <TableCell align="center">STT</TableCell>
                  <TableCell align="center">Sản phẩm</TableCell>
                  <TableCell align="center">Tên</TableCell>
                  <TableCell align="center">Loại</TableCell>
                  <TableCell align="center">Giá mua</TableCell>
                  <TableCell align="center">Giá ưu đãi</TableCell>
                  <TableCell align="center">Ngày update</TableCell>
                  <TableCell align="center">Thao tác</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products
                  ? products.map((row, index) => (
                    <StyledTableRow
                      key={index}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <StyledTableCell align="center">{index + 1}</StyledTableCell>
                      <StyledTableCell align="center">
                        <img
                          alt={'hình ' + index}
                          src={row.imageUrl[0]}
                          style={{ height: 40, width: 40 }}
                        ></img>
                      </StyledTableCell>
                      <StyledTableCell align="center">{row.name}</StyledTableCell>
                      <StyledTableCell align="center">{row.type}</StyledTableCell>
                      <StyledTableCell align="center">
                        {row.buyPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}vnđ
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.promotionPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}vnđ
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {dtfUS.format(new Date(row.timeUpdated))}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        <Button
                          value={index}
                          onClick={() => {
                            onBtnEditClick(row)
                          }}
                          style={{
                            borderRadius: 25,
                            backgroundColor: '#2196f3',
                            padding: '10px 20px',
                            fontSize: '10px',
                          }}
                          variant="contained"
                        >
                          Sửa
                        </Button>
                        <Button
                          value={index * index}
                          onClick={() => {
                            onBtnDeleteClick(row)
                          }}
                          style={{
                            borderRadius: 25,
                            backgroundColor: '#f50057',
                            padding: '10px 20px',
                            fontSize: '10px',
                          }}
                          variant="contained"
                        >
                          Xóa
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
                  : null}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>

      <ModalAddProduct
        varRefeshPage={varRefeshPage}
        openModalAdd={openModalAdd}
        handleClose={handleClose}
        getData={getData}
        setVarRefeshPage={setVarRefeshPage}
      />
      <ModalEditProduct
        varRefeshPage={varRefeshPage}
        openModalEdit={openModalEdit}
        handleCloseEdit={handleCloseEdit}
        getData={getData}
        setVarRefeshPage={setVarRefeshPage}
        rowClicked={rowClicked}
        setRowClicked={setRowClicked}
      />
      <ModalDeleteProduct varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} openModalDelete={openModalDelete} rowClickedDelete={rowClickedDelete} handleCloseDelete={handleCloseDelete} />
    </Container>
  )
}
export default AllProducts
