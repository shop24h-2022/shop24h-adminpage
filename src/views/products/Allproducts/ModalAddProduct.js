import {
  Alert,
  Box,
  Button,
  Input,
  MenuItem,
  Modal,
  Select,
  Snackbar,
  Typography,
} from '@mui/material'
import { useState } from 'react'
import { Col, Row } from 'reactstrap'
var subArrayImg = []
function ModalAddProduct({ varRefeshPage, openModalAdd, handleClose, getData, setVarRefeshPage }) {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    p: 4,
    borderRadius: '10px',
  }
  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('error')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const [arrayImages, setArrayImages] = useState(null)
  const [testState, setTestState] = useState(0) // biến tạm dùng để render
  const [img, setImg] = useState('')
  const [imgValueInput, setImgValueInput] = useState('')
  const [nameProduct, setNameProduct] = useState('')
  const [typeProduct, setTypeProduct] = useState('Fruits')
  const [buyPrice, setBuyPrice] = useState(0)
  const [promotionPrice, setPromotionPrice] = useState(0)
  const [description, setDescription] = useState('')
  const handleCancelModalClick = () => {
    handleClose()
    setArrayImages(null)
    subArrayImg = []
  }
  const onNameProductChange = (event) => {
    setNameProduct(event.target.value)
  }
  const onTypeProductChange = (event) => {
    setTypeProduct(event.target.value)
  }
  const onBuyPriceChange = (event) => {
    setBuyPrice(parseInt(event.target.value))
  }
  const onPromotionPriceChange = (event) => {
    setPromotionPrice(parseInt(event.target.value))
  }
  const onDescriptionChange = (event) => {
    setDescription(event.target.value)
  }
  const onImgChange = (event) => {
    setImg(event.target.value)
  }

  const onThemAnhClick = () => {
    if (subArrayImg.length == 4 && img !== '') {
      setOpenAlert(true)
      setNoidungAlertValid('Tối đa 4 ảnh')
      setStatusModal('error')
      document.getElementById('inputImgUrl').value = ''
      setImg('')
      return false
    }
    if (img !== '' && subArrayImg.includes(img) === false && subArrayImg.length < 5) {
      subArrayImg.push(img)
      setArrayImages(subArrayImg)
      setImg('')
      setImgValueInput('')
      console.log(subArrayImg)
      document.getElementById('inputImgUrl').value = ''
      setImg('')
      return true
    }
    if (subArrayImg.includes(img)) {
      setOpenAlert(true)
      setNoidungAlertValid('Ảnh này có rồi')
      setStatusModal('error')
      document.getElementById('inputImgUrl').value = ''
      setImg('')
    }
  }
  const onDeleteImgClick = (index) => {
    console.log(arrayImages)
    console.log(index)
    subArrayImg.splice(index, 1)
    setArrayImages(subArrayImg)
    console.log(subArrayImg)
    setTestState(testState + 1)
  }

  const onAddProductClick = () => {
    console.log('nameProduct', nameProduct)
    console.log('typeProduct', typeProduct)
    console.log('buyPrice', buyPrice)
    console.log('promotionPrice', promotionPrice)
    console.log('arr', arrayImages)
    console.log('description', description)
    var vCheck = validateForm()
    if (vCheck) {
      getAllproduct()
    }
  }
  const validateForm = () => {
    if (nameProduct === '') {
      setOpenAlert(true)
      setNoidungAlertValid('Tên sản phẩm cần được nhập')
      setStatusModal('error')
      return false
    }
    if (buyPrice <= 0) {
      setOpenAlert(true)
      setNoidungAlertValid('Giá mua không được âm hoặc bằng 0 hoặc không nhập ')
      setStatusModal('error')
      return false
    }
    if (promotionPrice <= 0) {
      setOpenAlert(true)
      setNoidungAlertValid('Giá ưu đãi không được âm hoặc bằng 0 hoặc không nhập ')
      setStatusModal('error')
      return false
    }
    if (arrayImages.length !== 3 && arrayImages.length !== 4) {
      setOpenAlert(true)
      setNoidungAlertValid('Số lượng ảnh là 3 hoặc 4')
      setStatusModal('error')
      return false
    }
    if (description === '') {
      setOpenAlert(true)
      setNoidungAlertValid('Nên để mô tả để khách hàng hiểu rõ sản phẩm')
      setStatusModal('error')
      return false
    } else {
      return true
    }
  }
  const getAllproduct = () => {
    getData('https://shop24h-backend.herokuapp.com/products')
      .then((data) => {
        console.log(data.products)
        var vCheck = true
        for (let i = 0; i < data.products.length; i++) {
          if (data.products[i].name.toLowerCase() == nameProduct.toLowerCase()) {
            setOpenAlert(true)
            setNoidungAlertValid('Trùng tên sản phẩm')
            setStatusModal('error')
            return (vCheck = false)
          }
        }
        if (vCheck) {
          getApiPostProduct()
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }
  const getApiPostProduct = () => {
    var infoProduct = {
      name: nameProduct,
      type: typeProduct,
      imageUrl: arrayImages,
      buyPrice: buyPrice,
      promotionPrice: promotionPrice,
      description: description,
    }
    const body = {
      method: 'POST',
      body: JSON.stringify(infoProduct),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    getData('https://shop24h-backend.herokuapp.com/products', body)
      .then((data) => {
        console.log(data)
        setOpenAlert(true)
        setNoidungAlertValid('Thêm mới sản phẩm thành công!')
        setStatusModal('success')
        setVarRefeshPage(varRefeshPage + 1)
        subArrayImg = []
        setArrayImages(null)
        handleClose()
      })
      .catch((error) => {
        console.log(error)
        setOpenAlert(true)
        setNoidungAlertValid('Lỗi thêm mới! Hãy thử lại!')
        setStatusModal('error')
      })
  }
  return (
    <>
      <Modal
        open={openModalAdd}
        onClose={handleCancelModalClick}
        aria-labelledby="modal-modal-info"
        aria-describedby="modal-modal-info"
      >
        <Box sx={style} style={{ backgroundColor: 'white' }}>
          <Typography mb={2} id="modal-modal-info" variant="h5" component="h2">
            <strong>Thêm sản phẩm</strong>
          </Typography>
          <br></br>
          <Row>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Tên sản phẩm:</label>
                    </Col>
                    <Col sm="8">
                      <Input onChange={onNameProductChange} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="5">
                      <label>Loại sản phẩm:</label>
                    </Col>
                    <Col sm="7">
                      <Select
                        id="type-product-select"
                        defaultValue="Fruits"
                        sx={{ minWidth: 168 }}
                        onChange={onTypeProductChange}
                      >
                        <MenuItem value="Fruits">Fruits</MenuItem>
                        <MenuItem value="Juice">Juice</MenuItem>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Giá mua:</label>
                    </Col>
                    <Col sm="8">
                      <Input type="number" onChange={onBuyPriceChange} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm="6">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="4">
                      <label>Giá ưu đãi:</label>
                    </Col>
                    <Col sm="8">
                      <Input type="number" onChange={onPromotionPriceChange} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col sm="12">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="2">
                      <label>Image URL:</label>
                    </Col>
                    <Col sm="10">
                      <Row>
                        <Col sm="12">
                          <Row>
                            <Col sm="9">
                              <Input
                                id="inputImgUrl"
                                style={{ width: '94%' }}
                                onChange={onImgChange}
                                placeholder="nhập URL vào đây, 3 hoặc 4 ảnh"
                              />
                            </Col>
                            <Col sm="3">
                              <Button
                                className="bg-warning w-75 text-white"
                                onClick={onThemAnhClick}
                              >
                                Thêm ảnh
                              </Button>
                            </Col>
                          </Row>
                          <Row className="mt-3">
                            {arrayImages
                              ? arrayImages.map((row, index) => (
                                  <Col sm="3" key={index}>
                                    <img
                                      alt={index}
                                      src={row}
                                      style={{ height: 60, width: 60 }}
                                    ></img>
                                    <span
                                      onClick={() => {
                                        onDeleteImgClick(index)
                                      }}
                                      type="button"
                                      className="badge badge-warning"
                                      id="lblCartCount"
                                    >
                                      {' '}
                                      x{' '}
                                    </span>
                                  </Col>
                                ))
                              : null}
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col sm="12">
              <Row>
                <Col sm="12">
                  <Row>
                    <Col sm="2">
                      <label>Mô tả:</label>
                    </Col>
                    <Col sm="10">
                      <textarea onChange={onDescriptionChange} style={{ width: '94%' }}></textarea>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <small style={{ fontSize: 12 }}>
              Hãy kiểm tra ảnh có hiển thị không trước khi click Thêm Sản Phẩm nhé!
            </small>
          </Row>
          <Row className="mt-4 text-center">
            <Col sm="12">
              <Row className="mt-4 mb-4">
                <Col sm="6">
                  <Button onClick={handleCancelModalClick} className="bg-success w-75 text-white">
                    Hủy Bỏ
                  </Button>
                </Col>
                <Col sm="6">
                  <Button onClick={onAddProductClick} className="bg-success w-75 text-white">
                    Thêm Sản Phẩm
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalAddProduct
