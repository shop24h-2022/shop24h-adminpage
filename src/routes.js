import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))




const AllProducts = React.lazy(() => import('./views/products/Allproducts'))
const AllCustomers = React.lazy(() => import('./views/customers/AllCustomers'))
const AllOrders = React.lazy(() => import('./views/orders/AllOrders'))
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', element: Dashboard },
  

  { path: '/products', name: 'All Products', element: AllProducts, exact: true },
  { path: '/customers', name: 'All Customers', element: AllCustomers, exact: true },
  { path: '/orders', name: 'All Orders', element: AllOrders, exact: true },
]

export default routes
